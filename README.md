<p align="center"><a href="http://desarrollo.tije.travel:7990/projects/DUO" target="_blank">
    <img src="http://desarrollo.tije.travel:7990/projects/DUO/avatar.png?s=128">
</a></p>

Autocomplete Services
==============

Author : Team APIDUO

How to install this project
---------------------------

  1. `git clone http://api-duo@desarrollo.tije.travel:7990/scm/duo/apiduo28.git`
  1. `cd apiduo28/`
  1. `composer install`
  1. `php app/console doctrine:mongodb:fixtures:load`
  1. `php app/console doctrine:mongodb:schema:create --index`
  1. `php app/console assets:install`
  1. `php app/console server:run`
  1. Browse `http://127.0.0.1:8000/admin/`

Command Symfony ODM (MongoDB)
---------------------------
Load Data in fixture (Load User admin:admin)

`php app/console doctrine:mongodb:fixtures:load`

Create index of schema in database
 
`php app/console doctrine:mongodb:schema:create --index`

Generating Getters and Setters

`php bin/console doctrine:mongodb:generate:documents AppBundle`


Command Symfony ORM (Mysql)
---------------------------
Create BD

`php bin/console doctrine:database:create`

Drop BD

`php bin/console doctrine:schema:drop --force`

Create Schema

`php app/console doctrine:schema:create`

Update Schema

`php bin/console doctrine:schema:update --force`

Create Bundle

`php app/console generate:bundle --namespace=ApiBundle`


Install MONGO DB
---------------------------

Doc : [Here][2]

Bundles App
---------------------------
* AppBundle: Backend, Documents, Admins, User and Group.
* ApiBundle: Definitions of interfaces.

[1]: http://desarrollo.tije.travel:7990/plugins/servlet/ssh/account/keys
[2]: https://docs.mongodb.com/master/tutorial/install-mongodb-on-ubuntu/?_ga=1.245350662.2086352550.1488568714

<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AutocompleteControllerTest extends WebTestCase
{
    public function testSearchGet()
    {
        $client = static::createClient();

        $method = 'GET';
        $url = '/autocomplete/search?flow_code=flights&product=flights&locale=es_AR&query=madrid';

        $headers = array(
            'HTTP_TOKEN' => 'qwerty'
        );

        $client->request($method, $url, array(), array(), $headers);

        $this->assertNotContains(
            'error',
            $client->getResponse()->getContent()
        );
    }
}
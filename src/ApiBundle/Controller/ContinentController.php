<?php

namespace ApiBundle\Controller;


use AppBundle\Document\Continent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use FOS\RestBundle\Controller\Annotations\View as Vw;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;

class ContinentController extends Controller
{
    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Return all continents",
     *     section="Continents"
     * )
     * @Get("/contienents/", name="continents")
     * @return array
     */
    public function getContinentsAction()
    {
        $continents = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Continent')
            ->findAll();

        $view = View::create();
        $view->setData($continents)->setStatusCode(200);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource = true,
     *  description = "Return one continent",
     *  section="Continents"
     * )
     * @Get("/contienent/", name="continents")
     * @param Continent $continent
     * @return array
     * @ParamConverter("user", class="AppBundle:Continent")
     */
    public function getContinentAction(Continent $continent)
    {
        $view = View::create();
        $view->setData($continent)->setStatusCode(200);
    }

}
<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;



class MasterpassController extends FOSRestController
{

    /**
     *
     * [
            {
                "callbackUrl":"http://localhost/vuelos/5/0/0/",
                "shoppingcart": {
                    "currency":"USD",
                    "shoppingcartItem":[
                        {
                            "quantity": 1,
                            "imageUrl":"http://localhost/vuelos/img.png",
                            "description":"Vuelo MIA-BUE",
                            "amount": 1052
                        }
                    ]
                }
            }
        ]
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Masterpass",
     *  description="Invoke Masterpass UI (Lightbox) for checkout.",
     * )
     * @Post("/lightbox/", name="lightbox")
     */
    public function lightboxAction(Request $request)
    {
        $view = View::create();
        $response = array();

        /*$token = $request->headers->get('token');

        if($token != 'qwerty')
        {
            $response['error'] = array (
                "code" => 403,
                "message" => "The apikey '" . $token . "' is invalid"
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }*/

        /*$em = $this->container->get('doctrine')->getManager();
        $token = $request->headers->get('token');
        $user = $this->getUserWithToken($token);
        $response = null;
        if(!$user)
        {
            $response['error'] = array (
                "code" => 403,
                "message" => "The apikey " . $token . " is invalid"
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }*/

        $data = json_decode($request->getContent(),true);

        if(json_last_error() != 0)
        {
            $response = array(
                'code' => 403,
                'message' => 'Invalid JSON decode'
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }

        if(count($data) != 1)
        {
            $response = array(
                'code' => 403,
                'message' => 'Invalid JSON decode'
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }

        //$view->setData($data)->setStatusCode(200);

        //return $view;

        $masterpass = $this->container->get('api.masterpass');

        $masterpass->setData($data[0]);

        $validate = $masterpass->validate();

        if($validate['error'])
        {
            $response['error'] = array (
                "code" => 400,
                "message" => $validate['message']
            );

            $view->setData($response)->setStatusCode(400);

            return $view;
        }

        $response = $masterpass->lightbox();

        $view->setData($response)->setStatusCode(200);

        return $view;
    }

    /**
     *
     * [
            {
                "mpstatus" = "success",
                "checkoutResourceUrl" = "",
                "oauthVerifier" = "",
                "oauthToken" = ""
            }
        ]
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Masterpass",
     *  description="Get Data Payment.",
     * )
     * @Post("/getDataPayment/", name="Data Payment")
     */
    public function getDataPaymentAction(Request $request)
    {
        $view = View::create();
        $response = array();

        $data = json_decode($request->getContent(),true);

        if(json_last_error() != 0)
        {
            $response = array(
                'code' => 403,
                'message' => 'Invalid JSON decode'
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }

        if(count($data) != 1)
        {
            $response = array(
                'code' => 403,
                'message' => 'Invalid JSON decode'
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }

        $masterpass = $this->container->get('api.masterpass');

        $masterpass->setCallback($data[0]);

        $validate = $masterpass->validateCallback();

        if($validate['error'])
        {
            $response['error'] = array (
                "code" => 400,
                "message" => $validate['message']
            );

            $view->setData($response)->setStatusCode(400);

            return $view;
        }

        $response = $masterpass->dataPayment();

        $view->setData($response)->setStatusCode(200);

        return $view;
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Masterpass",
     *  description="Log Transaction.",
     * )
     * @Post("/logTransaction/", name="Log Transaction")
     */
    public function logTransactionAction(Request $request)
    {
        $view = View::create();
        $response = array();

        $data = json_decode($request->getContent(),true);

        //Chekeamos si es un json valido
        if(json_last_error() != 0)
        {
            $response["error"] = array(
                "error_status" => true,
                "error_code" => 403,
                "error_message" => "Invalid JSON decode"
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }

        //Chekeamos si es un json valido
        if(count($data) != 1)
        {
            $response["error"] = array(
                "error_status" => true,
                "error_code" => 403,
                "error_message" => "Invalid JSON decode"
            );

            $view->setData($response)->setStatusCode(403);

            return $view;
        }
        //Obtenemos el servicio
        $masterpass = $this->container->get('api.masterpass');

        $masterpass->setTransactionLog($data[0]);

        $validate = $masterpass->validateTransactionLog();

        if($validate["error"])
        {
            $response["error"] = array (
                "error_status" => true,
                "error_code" => 400,
                "error_message" => $validate['message']
            );

            $view->setData($response)->setStatusCode(400);

            return $view;
        }

        $response = $masterpass->logTransaction();

        $view->setData(array(
            "error" => array(
                "error_status" => false,
                "error_code" => null,
                "error_message" => null
            ),
            "result" => $response
        ))->setStatusCode(200);

        return $view;
    }

}
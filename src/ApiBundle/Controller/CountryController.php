<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View as AnnotationsView;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Document\Country;
use ApiBundle\Form\Type\CountryType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;

class CountryController extends FOSRestController
{

    /**
     * Get all the Countries
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets all Country",
     *   output = "AppBundle\Document\Country",
     *   section="Country",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @return array
     * @AnnotationsView()
     * @Get("/countries")
     */
    public function getCountriesAction(){

        $countries = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Country')
            ->findAll();

        return array('countries' => $countries);
    }

    /**
     * REST action which returns type by id.
     * Method: GET, url: /api/continents/{id}.{_format}
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a Country for a given id",
     *   output = "AppBundle\Document\Country",
     *   section="Country",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @param $id
     * @return mixed
     */
    public function getCountryAction($id) {
        /** @var TypeRepository $typeRepository */

        $typeRepository = $this->get('doctrine_mongodb')->getRepository('AppBundle:Country');
        $type = NULL;
        try {
            $type = $typeRepository->find($id);
        } catch (\Exception $exception) {
            $type = NULL;
        }

        if (!$type) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }

        $view = View::create();
        $view->setData($type)->setStatusCode(200);

        return $view;
    }


}
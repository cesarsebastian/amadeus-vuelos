<?php

namespace ApiBundle\Controller;

/*use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View as AnnotationsView;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Document\Country;
use ApiBundle\Form\Type\CountryType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;*/

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class RouteController extends FOSRestController
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets Routes for a given token od client",
     *   section = "Routes",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @return View
     */
    public function routeAction(Request $request)
    {
        $response = array();
        $tokenValue = $request->headers->get('token');

        // servicio que valida autenticidad de cliente
        $token = $this->get('api.utils');
        $validate = $token->validateClient($tokenValue);

        if($validate['data']['error']) {

            $response['error'] = array(
                'message' => $validate['data']['message']
            );

            $view = View::create();
            $view->setData($response)->setStatusCode(400);

            return $view;
        }

        $client_id = $validate['data']['client_id'];
        $dm = $this->get('doctrine_mongodb')->getManager();
        $client = $dm->createQueryBuilder('AppBundle:Client')
                    ->field('id')->equals($client_id)
                    ->getQuery()
                    ->getSingleResult();
        $routes = $client->getRoutes();

        if(empty($routes)) {

            $response['error'] = array(
                'message' => "sorry, there is not route for this client"
            );

            $view = View::create();
            $view->setData($response)->setStatusCode(400);

            return $view;
        }

        foreach($routes as $route){

            $clientRoutes[] = array(
                'origin_city' => $route->getOriginCity()->getName(),
                'origin_iata'=> $route->getOriginCity()->getIata(),
                'destination_city' => $route->getDestinationCity()->getName(),
                'destination_iata'=> $route->getDestinationCity()->getIata()
            );
        }

        $response['response'] = $clientRoutes;
        $view = View::create();
        $view->setData($response)->setStatusCode(200);

        return $view;
    }


}
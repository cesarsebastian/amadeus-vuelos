<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends FOSRestController
{
   /**
     * Busqueda de vuelos
     *
     * @ApiDoc(
     *  resource = true,
     *  description = "Busqueda de vuelos",
     *  requirements={
     *   {"name"="type", "dataType"="string", "requirement"="oneway|roundtrip|multidestination", "description"="Tipo de vuelo"},
     *   {"name"="cityFromId", "dataType"="string", "requirement"="Codigo Iata", "description"="Origen"},
     *   {"name"="cityToId", "dataType"="string", "requirement"="Codigo Iata", "description"="Destino"},
     *   {"name"="dateFrom", "dataType"="string", "requirement"="ddmmyy", "description"="Fecha de ida"},
     *   {"name"="dateTo", "dataType"="integer", "requirement"="ddmmyy", "description"="Fecha de vuelta"},
     *   {"name"="adults", "dataType"="integer", "requirement"="\d+", "description"="Cantidad de adultos"},
     *   {"name"="childrens", "dataType"="integer", "requirement"="\d+", "description"="Cantidad de niños"},
     *   {"name"="infants", "dataType"="integer", "requirement"="\d+", "description"="Cantidad de infantes"}
     *  },
     *  statusCodes = {
     *   200 = "Returned when successful"
     *  }
     * )
     *
     * @Get("/search/{type}/{cityFromId}/{cityToId}/{dateFrom}/{dateTo}/{adults}/{childrens}/{infants}/", name="getSearch")
     *
     * @return View
     */
   public function getSearchAction(Request $request, $type, $cityFromId, $cityToId, $dateFrom, $dateTo, $adults, $childrens, $infants) {
        $token = $request->headers->get('token');

//        if($token==NULL) {
//            $token = '9dIBIV/ZG3a/ojWuvRaiAkl+rr3aI4cjpQOTGnNLMLA=';
//        }

        list($proveedor, $sitio) = explode('-', $this->decrypt($token));
        preg_match('/\w+/', $sitio, $sitio);
        $sitio = $sitio[0];

        $clients = array(
            'tije' => array(
                'turismocity' => '9dIBIV/ZG3a/ojWuvRaiAkl+rr3aI4cjpQOTGnNLMLA=',
                'tudestino'   => 'awahaIVjZqfJdUG6xQE7arNjhqYXqg5hohW5s/Lzn5o=',
                'viajala'     => 'KrShzFsMdeZVY4M4LAkeE/0+WUu3UA1458W9sguS3Fs=',
                'skyscanner'  => 'xuxvd35LiuqkeKfwmcwqlZN+yleddr9PY3GKuyX4Y2A=',
                'buscoaereos' => 'XTjZ4PrkFJTcPQuivItfN+fPrMjcyPXvdEqiP23NSuI=',
                'volemos'     => 'gGBfo3MHGtTSLmrVg4uIdzSrti19iKfzPO2nRbMXKRc=',
            ),
        );

        $fonts = array(
            'tije' => array(
                'turismocity' => '?utm_source=turismocity&utm_medium=post_afiliados&utm_campaign=turismocity',
                'tudestino'   => '?utm_source=tudestino&utm_medium=post_afiliados&utm_campaign=tudestino',
                'viajala'     => '?utm_source=viajala&utm_medium=post_afiliados&utm_campaign=viajala',
                'skyscanner'  => '?utm_source=skyscanner&utm_medium=post_afiliados&utm_campaign=skyscanner',
                'buscoaereos' => '?utm_source=buscoaereos&utm_campaign=buscoaereos&utm_medium=cpc',
                'volemos'     => '?utm_source=volemos&utm_medium=post_afiliados&utm_campaign=volemos',
            ),
        );

        $response = array();

        if(isset($clients[$proveedor][$sitio]) and ($token==$clients[$proveedor][$sitio]) and ($proveedor=='tije')) {
            if($sitio=='turismocity') {//Validar sitio TurismoCity
                $enable = false;
                //Lu a Do de 9 a 13 hs, de 14 a 18 hs, de 19 a 00 hs (23:59)
                $fecha = new \DateTime();
                $hora  = $fecha->format('H');
                if (($hora >= '09' && $hora < '13') || ($hora >= '14' && $hora < '18') || ($hora >= '19' && $hora <= '23')) {
                    $turismoCityValidate = $this->getDoctrine()->getManager()->getRepository('BackendBundle:TurismoCityValidate')->find(1);
                    $enable = $turismoCityValidate->getEnable();
                }
            } else {
                $enable = true;
            }

            if($enable) {
                $dateFechaFutura = explode(',', $dateFrom);
                $dateFechaFutura = $dateFechaFutura[0];
                $dateFuturo = \DateTime::createFromFormat('dmy', $dateFechaFutura);
                $dateNow = new \DateTime();
                $diff = $dateNow->diff($dateFuturo);

                if ($diff->format('%a') >= 3) {
                    $request = $this->getRequest();
                    $request->request->set(      'type', strtolower($type));
                    $request->request->set('cityFromId', strtoupper($cityFromId));
                    $request->request->set(  'cityToId', strtoupper($cityToId));
                    $request->request->set(  'dateFrom', $dateFrom);
                    $request->request->set(    'dateTo', $dateTo);
                    $request->request->set(    'adults', $adults);
                    $request->request->set( 'childrens', $childrens);
                    $request->request->set(   'infants', $infants);

                    $analitics = isset($fonts[$proveedor][$sitio]) ? $fonts[$proveedor][$sitio] : NULL;
                    $service = array(
                        'tije' => array(
                            'siteId' => 1,
                            'processor' => 'core.api.netviax.processor',
                            'client' => $fonts[$proveedor][$sitio],
                            'options' => null,
                        ),
                    );

                    $processor = $this->container->get($service[$proveedor]['processor']);

                    $response = $processor->searchApiJsonResponse($request, $service[$proveedor]['client'], $service[$proveedor]['siteId'], $service[$proveedor]['options']);
                    $response = $response->getContent();

                    $response = json_decode($response, true);
                } else {
                    $response = [];
                }
            } else {
                $response = [];
            }
        } else {
            $response['error'] = ['message' => 'Token invalido'];
        }

        $view = View::create();
        $view->setData($response)->setStatusCode(200);

        return $view;
    }

    private function encrypt($data)
    {
        # --- ENCRYPTION ---

        # la clave debería ser binaria aleatoria, use scrypt, bcrypt o PBKDF2 para
        # convertir un string en una clave
        # la clave se especifica en formato hexadecimal
        $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");

        # crear una aleatoria IV para utilizarla co condificación CBC
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        # crea un texto cifrado compatible con AES (tamaño de bloque Rijndael = 128)
        # para hacer el texto confidencial
        # solamente disponible para entradas codificadas que nunca finalizan con el
        # el valor  00h (debido al relleno con ceros)
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);

        # anteponer la IV para que esté disponible para el descifrado
        $ciphertext = $iv . $ciphertext;

        # codificar el texto cifrado resultante para que pueda ser representado por un string
        $ciphertext_base64 = base64_encode($ciphertext);

        return $ciphertext_base64;
    }

    private function decrypt($data)
    {
        # --- DECRYPTION ---

        # la clave debería ser binaria aleatoria, use scrypt, bcrypt o PBKDF2 para
        # convertir un string en una clave
        # la clave se especifica en formato hexadecimal
        $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");

        # crear una aleatoria IV para utilizarla co condificación CBC
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $ciphertext_dec = base64_decode($data);

        # recupera la IV, iv_size debería crearse usando mcrypt_get_iv_size()
        $iv_dec = substr($ciphertext_dec, 0, $iv_size);

        # recupera el texto cifrado
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);

        # podrían eliminarse los caracteres con valor 00h del final del texto puro
        $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

        return $plaintext_dec;
    }
}
//
//    /**
//     * @Route("/search/")
//     */
//    public function searchAction()
//    {   
//        
//        $data = array("Usuarios" => array(
//        array(
//            "nombre"   => "Víctor",
//            "Apellido" => "Robles"
//        ),
//        array(
//            "nombre"   => "Antonio",
//            "Apellido" => "Martinez"
//        )));
//        
//        return $data;
//    }


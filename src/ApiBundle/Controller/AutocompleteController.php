<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;



class AutocompleteController extends FOSRestController
{
   /**
     * Autocomplete services
     *
     * @ApiDoc(
     *  resource = true,
     *  description = "Autocomplete services V0.1",
     *  section = "Autocomplete",
     *  parameters = {
     *      {"name"="flow_code", "dataType"="string", "required"=true, "description"="flight_hotel, flight, car, hotel, flight_car"},
     *      {"name"="product", "dataType"="string", "required"=true, "description"="flights, car, hotel, packages"},
     *      {"name"="locale", "dataType"="string", "required"=true, "description"="String which contains language and country. (es_AR)"},
     *      {"name"="query", "dataType"="string", "required"=true, "description"="The text to be looked for (4...n)"},
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      403 = "Token invalid",
     *      400 = "Returned when get data has errors"
     *  }
     * )
     * @Get("/search", name="autocomplete")
     * @return View
     */

   public function getDataSearchAction(Request $request)
   {
       $view = View::create();

       $response = array();
       /*$token = $request->headers->get('token');

       if($token != 'qwerty')
       {
           $response['error'] = array (
                "code" => 403,
                "message" => "The apikey " . $token . " is invalid"
           );

           $view->setData($response)->setStatusCode(403);

           return $view;
       }*/

       $dataRequest = array();

       $dataRequest['query'] = $request->get('query');
       $dataRequest['product'] = $request->get('product');
       $dataRequest['locale'] = $request->get('locale');
       $dataRequest['flow_code'] = $request->get('flow_code');

       $processor = $this->container->get('api.autocomplete');

       $validate = $processor->validate($dataRequest);

       if($validate['error'])
       {
           $response['error'] = array (
               "code" => 400,
               "message" => $validate['message']
           );

           $view->setData($response)->setStatusCode(400);

           return $view;
       }

       //Con data seteada
       $response = $processor->getDataSearch($validate['data_request']);

       $view->setData(array('data' => $response))->setStatusCode(200);

       return $view;
    }
}
<?php

namespace ApiBundle\Utils;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;


class Utils
{
    public function __construct($entityManager, $container)
    {
        $this->em        = $entityManager;
        $this->container = $container;
    }
    /*
     * $getToken = $this->em->getRepository('AppBundle:Token')->findOneBy(array("value" => $value));
     * $getToken = $this->em->getRepository('AppBundle:Token')->findOneByValue($value);
     * $client = serialize($client);
     * */
    public function validateClient($token)
    {
        $value = $token;

        //busca en BD el token del cliente
        $getToken = $this->em
            ->createQueryBuilder('AppBundle:Token')
            ->field('value')->equals($value)
            ->getQuery()
            ->getSingleResult();

        $response = array();

        if(!empty($getToken)) {

            $client = $this->em
                ->createQueryBuilder('AppBundle:Client')
                ->field('tokens')->references($getToken)
                ->getQuery()
                ->getSingleResult();

            $active = $client->getActive();

            if($active){

                $urlOrigin = $client->getOriginUrl();

                if($this->container->get('request')->getClientIp() == $urlOrigin){

                    //respuesta correcta

                    $response['data'] = array(
                        'error' => 0,
                        'message' => '',
                        'client_id' => $client->getId()
                    );

                }else{
                    
                    //url invalida

                    $response['data'] = array(
                        'error' => 1,
                        'message' => 'invalid url',
                        'client_id' => null
                    );
                }

            }else{
                //client inactivo

                $response['data'] = array(
                    'error' => 1,
                    'message' => 'inactive client',
                    'client_id' => null
                );
            }

        }else{
            //token invalido
            $response['data'] = array(
                'error' => 1,
                'message' => 'invalid token',
                'client_id' => null
            );

        }

        return $response;
    }

}
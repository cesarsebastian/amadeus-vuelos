<?php

namespace ApiBundle\Service;

use Hoya\MasterpassBundle\DTO\CallbackResponse;
use Hoya\MasterpassBundle\DTO\MerchantInit;
use Hoya\MasterpassBundle\DTO\Shoppingcart;
use Hoya\MasterpassBundle\DTO\ShoppingcartItem;
use Hoya\MasterpassBundle\DTO\Transaction;
use Symfony\Component\HttpFoundation\Request;

class Masterpass
{
    private $container = null;

    protected $requestToken;
    protected $cartXml;
    protected $responseXml;
    protected $data;
    protected $callback;

    protected $mpstatus;
    protected $checkoutResourceUrl;
    protected $oauthVerifier;
    protected $oauthToken;

    protected $transactionLog;

    public function __construct($container = null)
    {
       $this->container = $container;
    }

    public function validate()
    {
        $main = array(
            array('field' => 'callbackUrl', 'type' => 'url'),
            array('field' => 'shoppingcart', 'type' => 'object')
        );

        $dd = $this->getData();

        $validation = $this->validateJson($dd, $main);

        if($validation)
        {
            return array(
                'error' => true,
                'message' => $validation
            );
        }

        $shoppingcart = array(
            array('field' => 'currency', 'type' => 'string'),
            array('field' => 'shoppingcartItem', 'type' => 'array')
        );

        $validation = $this->validateJson($dd['shoppingcart'], $shoppingcart);

        if($validation)
        {
            return array(
                'error' => true,
                'message' => 'shoppingcart: ' . $validation
            );
        }

        $shoppingcartItem = array(
            array('field' => 'quantity', 'type' => 'integer'),
            array('field' => 'imageUrl', 'type' => 'url'),
            array('field' => 'description', 'type' => 'string'),
            array('field' => 'amount', 'type' => 'string')
        );

        foreach ($dd['shoppingcart']['shoppingcartItem'] as $item)
        {
            $validation = $this->validateJson($item, $shoppingcartItem);

            if($validation)
            {
                return array(
                    'error' => true,
                    'message' => 'shoppingcartItem: ' . $validation
                );
            }
        }

        return array(
            'error' => false,
            'message' => 'Success valid fields'
        );

    }

    public function validateCallback()
    {
        $main = array(
            array('field' => 'mpstatus', 'type' => 'string'),
            array('field' => 'checkoutResourceUrl', 'type' => 'url'),
            array('field' => 'oauthVerifier', 'type' => 'string'),
            array('field' => 'oauthToken', 'type' => 'string')
        );

        $data = array(
            'mpstatus' => $this->getMpstatus(),
            'checkoutResourceUrl' => $this->getCheckoutResourceUrl(),
            'oauthVerifier' => $this->getOauthVerifier(),
            'oauthToken' => $this->getOauthToken()

        );

        $validation = $this->validateJson($data, $main);

        if($validation)
        {
            return array(
                'error' => true,
                'message' => $validation
            );
        }

        return array(
            'error' => false,
            'message' => 'Success valid fields'
        );

    }

    public function lightbox()
    {
        $this->requestTokenService();
        $this->shoppingCart();
        $this->merchantInit();

        $dd = $this->getData();

        $result = array(
            'requestToken' => $this->requestToken->requestToken,
            'authorizeUrl' => $this->requestToken->authorizeUrl,
            'callbackConfirmed' => $this->requestToken->callbackConfirmed,
            'oAuthExpiresIn' => $this->requestToken->oAuthExpiresIn,
            'oAuthSecret' => $this->requestToken->oAuthSecret,
            'redirectUrl' => $this->requestToken->redirectUrl,
            'callbackUrl' => $dd['callbackUrl'],
            'merchantCheckoutId' => "6f339f0e31f746f4ab00f5beb38f6965"
        );

        return $result;
    }

    public function dataPayment()
    {
        $this->service = $this->container->get('hoya_masterpass_service');

        $callback = new CallbackResponse();
        $callback->mpstatus = $this->getMpstatus();
        $callback->checkoutResourceUrl = $this->getCheckoutResourceUrl();
        $callback->oauthVerifier = $this->getOauthVerifier();
        $callback->oauthToken = $this->getOauthToken();

        $accessTokenResponse = $this->service->getAccessToken($callback);

        $checkoutData = $this->service->getCheckoutData($accessTokenResponse);

        $xml_object = simplexml_load_string($checkoutData);

        $xml_array = $this->object2array($xml_object);


        return array(
            'data' => $xml_array
            /*'billingAddress' => $checkoutData->Address,
            'contact' => $checkoutData->Contact,
            'authOptions' => $checkoutData->AuthenticationOptions,
            'preCheckoutTransactionId' => $checkoutData->PreCheckoutTransactionId,
            'rewardProgram' => $checkoutData->RewardProgram,
            'shippingAddress' => $checkoutData->ShippingAddress,
            'transactionId' => $checkoutData->TransactionId,
            'walletId' => $checkoutData ->WalletID*/
        );
    }

    public function logTransaction()
    {
        $this->service = $this->container->get('hoya_masterpass_service');

        $purchase = new \DateTime;

        $tl = $this->getTransactionLog();

        $transaction = new Transaction();
        $transaction->transactionId = "sw-n30t-ENffjpOBvdlYZC10ngDBhJAQZMDiFjIn15e323de!347779dcc7db4018ad1bf5b2c7a5c8ac0000000000000000";//449087232;
        $transaction->consumerKey = $tl['consumerKey']; //'cLb0tKkEJhGTITp_6ltDIibO5Wgbx4rIldeXM_jRd4b0476c!414f4859446c4a366c726a327474695545332b353049303d';
        $transaction->currency = $tl['currency'];//'USD';
        $transaction->setAmount($tl['amount']);//(1.00);
        $transaction->transactionStatus = $tl['transactionStatus'];//'Success';
        $transaction->setPurchaseDate($purchase);//($tl['purchaseDate']);//($purchase);
        $transaction->approvalCode = $tl['approvalCode'];

        $response = $this->service->postTransaction($transaction);

        return $response;
    }


    private function requestTokenService()
    {
        $this->service = $this->container->get('hoya_masterpass_service');

        $this->requestToken = $this->service->getRequestToken();
    }

    private function shoppingCart()
    {
        $cart = new Shoppingcart();
        $cart->currency = $this->data['shoppingcart']['currency'];
        $i = 1;

        foreach ($this->data['shoppingcart']['shoppingcartItem'] as $it)
        {

            $item = new ShoppingcartItem();
            $item->quantity = $it['quantity'];
            $item->imageUrl = $it['imageUrl'];
            $item->description = $it['description'];
            $item->setAmount($it['amount']);

            $cart->addItem($i, $item);
            $i++;
        }

        $shoppingCartXml = $cart->toXML();

        $this->cartXml = $this->service->postShoppingCartData($this->requestToken, $shoppingCartXml);

    }

    private function merchantInit()
    {
        $init = new MerchantInit();
        $initXml = $init->toXML();

        $this->responseXml = $this->service->postMerchantInitData($this->requestToken, $initXml);
    }

    private function validateJson($data,$fields)
    {
        foreach ($fields as $field) {
            if (isset($data[$field['field']])) {
                if ($data[$field['field']] != '') {
                    if ($field['type'] == 'date') {
                        if (!$this->validateDate($data[$field['field']])) {
                            return 'Invalid date format of "' . $field['field'] . ".";
                        }
                    }
                    if ($field['type'] == 'array') {
                        if (!is_array($data[$field['field']])) {
                            return 'Incomplete node of "' . $field['field'] . '"';
                        }
                    }
                    if ($field['type'] == 'integer') {
                        if (!is_int($data[$field['field']])) {
                            return '"' . $field['field'] . '" expects an integer';
                        }
                    }
                    if ($field['type'] == 'email') {
                        if (!filter_var($data[$field['field']], FILTER_VALIDATE_EMAIL)) {
                            return '"' . $field['field'] . '" expects an email';
                        }
                    }
                    if ($field['type'] == 'url') {
                        if (!filter_var($data[$field['field']], FILTER_VALIDATE_URL)) {
                            return '"' . $field['field'] . '" expects an URL';
                        }
                    }
                } else {
                    return 'The field "' . $field['field'] . '" is required';
                }
            } else {
                return 'The field "' . $field['field'] . '" is required';
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param mixed $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;

        $this->setMpstatus($callback['mpstatus']);
        $this->setCheckoutResourceUrl($callback['checkout_resource_url']);
        $this->setOauthVerifier($callback['oauth_verifier']);
        $this->setOauthToken($callback['oauth_token']);

    }

    /**
     * @return mixed
     */
    public function getMpstatus()
    {
        return $this->mpstatus;
    }

    /**
     * @param mixed $mpstatus
     */
    public function setMpstatus($mpstatus)
    {
        $this->mpstatus = $mpstatus;
    }

    /**
     * @return mixed
     */
    public function getCheckoutResourceUrl()
    {
        return $this->checkoutResourceUrl;
    }

    /**
     * @param mixed $checkoutResourceUrl
     */
    public function setCheckoutResourceUrl($checkoutResourceUrl)
    {
        $this->checkoutResourceUrl = $checkoutResourceUrl;
    }

    /**
     * @return mixed
     */
    public function getOauthVerifier()
    {
        return $this->oauthVerifier;
    }

    /**
     * @param mixed $oauthVerifier
     */
    public function setOauthVerifier($oauthVerifier)
    {
        $this->oauthVerifier = $oauthVerifier;
    }

    /**
     * @return mixed
     */
    public function getOauthToken()
    {
        return $this->oauthToken;
    }

    /**
     * @param mixed $oauthToken
     */
    public function setOauthToken($oauthToken)
    {
        $this->oauthToken = $oauthToken;
    }


    private function object2array($object)
    {
        return @json_decode(@json_encode($object),1);
    }

    /**
     * @return mixed
     */
    public function getTransactionLog()
    {
        return $this->transactionLog;
    }

    /**
     * @param mixed $transactionLog
     */
    public function setTransactionLog($transactionLog)
    {
        $this->transactionLog = $transactionLog;
    }

    public function validateTransactionLog()
    {
        $main = array(
            //array('field' => 'transactionId', 'type' => 'int'),
            array('field' => 'consumerKey', 'type' => 'string'),
            array('field' => 'currency', 'type' => 'string'),
            array('field' => 'amount', 'type' => 'string'),
            array('field' => 'transactionStatus', 'type' => 'string'),
            //array('field' => 'purchaseDate', 'type' => 'string'),
            array('field' => 'approvalCode', 'type' => 'string')
        );

        /*$data = array(
            'mpstatus' => $this->getMpstatus(),
            'checkoutResourceUrl' => $this->getCheckoutResourceUrl(),
            'oauthVerifier' => $this->getOauthVerifier(),
            'oauthToken' => $this->getOauthToken()

        );*/

        $validation = $this->validateJson($this->getTransactionLog(), $main);

        if($validation)
        {
            return array(
                'error' => true,
                'message' => $validation
            );
        }

        return array(
            'error' => false,
            'message' => 'Success valid fields'
        );
    }

}
<?php

namespace ApiBundle\Service;

class Autocomplete
{
    private $products = array('flight','car','hotel','packages');
    private $flows_code = array('flight','car','hotel','flight_hotel','flight_car');
    private $locales = array('es_ar');

    private $container = null;

    public function __construct($container = null)
    {
       $this->container = $container;
    }

    public function validate($dataRequest)
    {
        if (!in_array($flow_code = strtolower(trim($dataRequest['flow_code'])),$this->flows_code))
        {
            return array(
                'error' => true,
                'message' => 'The field flow_code: ' . $flow_code . ' is invalid',
                'dataRequest' => null
            );
        }

        if (!in_array($locale = strtolower(trim($dataRequest['locale'])),$this->locales))
        {
            return array(
                'error' => true,
                'message' => 'The field locale: ' . $locale . ' is invalid',
                'dataRequest' => null
            );
        }

        if (!in_array($product = strtolower(trim($dataRequest['product'])),$this->products))
        {
            return array(
                'error' => true,
                'message' => 'The field flow_code: ' . $product . ' is invalid',
                'dataRequest' => null
            );
        }
        $dataRequestNew = array();
        $dataRequestNew['query'] = trim($dataRequest['query']);
        $dataRequestNew['product'] = $product;
        $dataRequestNew['locale'] = $locale;
        $dataRequestNew['flow_code'] = $flow_code;

        return array(
            'error' => false,
            'message' => 'Success valid fields',
            'data_request' => $dataRequestNew
        );

    }

    public function getDataSearch($dataRequest)
    {
        $query = $dataRequest['query'];
        $product = $dataRequest['product'];
        $locale = $dataRequest['locale'];
        $flow_code = $dataRequest['flow_code'];

        $nameService = 'api.autocomplete.'.$flow_code;

        $processor= $this->container->get($nameService);

        return $processor->explore($query);
    }
}
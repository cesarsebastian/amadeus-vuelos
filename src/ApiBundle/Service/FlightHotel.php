<?php

namespace ApiBundle\Service;

class FlightHotel
{
    private $container = null;

    public function __construct($container = null)
    {
        $this->container = $container;
    }

    public function explore($query)
    {

        $response = array();
        $citiesResult = array();
        $zonesResult = array();

        //Search in cities

        $finder = $this->container->get('fos_elastica.finder.app.city');

        $cities = $finder->find($query);

        foreach ($cities as $city)
        {
            $citiesResult[] = array (
                '_objectId' => $city->getId(),
                'name' => $city->getName(),
                //'iata' => $city->getIata(),
                //'iata-from-hotel' => $city->getIataFromHotel(),
                //'name-from-hotel' => $city->getNameFromHotel(),
                //'iata-from-aereo' => $city->getIataFromAereo(),
                //'name-from-aereo' => $city->getNameFromAereo(),
                'hotel-question' => '0-' . $city->getIdCityOld(),
                'aereo-question' => ($city->getIata()) ? $city->getIata() : (($city->getIataFromAereo()) ? $city->getIataFromAereo() : (($city->getIataFromHotel()) ? $city->getIataFromHotel() : ''))
            );
        }


        //Search zones

        $finder = $this->container->get('fos_elastica.finder.app.zone');

        $zones = $finder->find($query);

        foreach($zones as $zone)
        {
            $zonesResult[] = array(
                '_objectId' => $zone->getId(),
                'name' => $zone->getName(),
                'description' => $zone->getDescription(),
                'label' => ($zone->getCity()) ? $zone->getCity()->getName() .' - '. $zone->getName() : 'null' .'-'. $zone->getName(),
                //'hotel-question' => app/Resources/NelmioApiDocBundle/views/jsonExampleResponse.html.twig
                'hotel-question' => $zone->getIdZoneOld() . '-' . (($zone->getCity()) ? $zone->getCity()->getIdCityOld() : '0'),
                'aereo-question' => ($zone->getAirport())? $zone->getAirport()->getIata() : (($zone->getCity()) ? $zone->getCity()->getIata() : '')
            );
        }

        $response['CITIES'] = $citiesResult;
        $response['ZONES'] = $zonesResult;

        return $response;
    }
}
<?php

namespace ApiBundle\Service;

class Flight
{
    private $container = null;

    public function __construct($container = null)
    {
        $this->container = $container;
    }

    public function explore($query)
    {
        $response = array();
        $citiesResult = array();
        $airportsResult = array();

        //Search in cities

        $finder = $this->container->get('fos_elastica.finder.app.city');
        $cities_elastic = $finder->find($query);

        //return $cities_elastic;

        foreach ($cities_elastic as $city)
        {
            $citiesResult[] = array(
                'objectId' => $city->getId(),
                'name' => $city->getName(),
                'iata' => $city->getIata(),
                'label' => $city->getNameLabel()
            );

            if($city->getAirports())
            {
                foreach ($city->getAirports() as $airport)
                {
                    $airportsResult[] = array(
                        'objectId' => $airport->getId(),
                        'name' => $airport->getName(),
                        'iata' => $airport->getIata()
                    );
                }
            }
        }

        //Search in airports

        $finder = $this->container->get('fos_elastica.finder.app.airport');
        $airports_elastic = $finder->find($query);

        foreach ($airports_elastic as $airport)
        {
            $airportsResult[] = array(
                'objectId' => $airport->getId(),
                'name' => $airport->getName(),
                'iata' => $airport->getIata()
            );
        }

        $response['CITIES'] = $citiesResult;
        $response['AIRPORTS'] = $airportsResult;

        return $response;
    }
}
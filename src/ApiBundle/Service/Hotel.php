<?php

namespace ApiBundle\Service;

class Hotel
{
    private $container = null;

    public function __construct($container = null)
    {
        $this->container = $container;
    }

    public function explore($query)
    {

        $response = array();
        $citiesResult = array();
        $zonesResult = array();
        $hotelsResult = array();

        //Search in cities

        $finder = $this->container->get('fos_elastica.finder.app.city');
        $cities_elastic = $finder->find($query);

        foreach ($cities_elastic as $city)
        {
            $citiesResult[] = array(
                'objectId' => $city->getId(),
                'name' => $city->getName(),
                'code' => '0-' . $city->getIdCityOld(),
                'label' => $city->getNameLabel()
            );
        }

        //Search in zones

        $finder = $this->container->get('fos_elastica.finder.app.zone');
        $zones_elastic = $finder->find($query);

        foreach ($zones_elastic as $zone)
        {
            $zonesResult[] = array(
                'objectId' => $zone->getId(),
                'name' => $zone->getName(),
                'description' => $zone->getDescription(),
                'code' => $zone->getIdZoneOld() . '-' . (($zone->getCity()) ? $zone->getCity()->getIdCityOld() : '0')
            );

            if($zone->getCity())
            {
                $citiesResult[] = array(
                    'objectId' => $zone->getCity()->getId(),
                    'name' => $zone->getCity()->getName(),
                    'code' => '0-' . $zone->getCity()->getIdCityOld(),
                    'label' => $zone->getCity()->getNameLabel()
                );
            }
        }

        // Search in hotels

        $finder = $this->container->get('fos_elastica.finder.app.hotel');
        $hotels_elactic = $finder->find($query);

        foreach ($hotels_elactic as $hotel)
        {

            $hotelsResult[] = array(
                'objectId' => $hotel->getId(),
                'name' => $hotel->getName(),
                'address' => $hotel->getDireccion(),
                'lat' => $hotel->getLat(),
                'lon' => $hotel->getLon(),
                'code' => $hotel->getCode()
            );

            if($hotel->getCity())
            {
                $citiesResult[] = array(
                    'objectId' => $hotel->getCity()->getId(),
                    'name' => $hotel->getCity()->getName(),
                    'iata' => $hotel->getCity()->getIata(),
                    'label' => $hotel->getCity()->getNameLabel()
                );
            }

            if($hotel->getZone())
            {
                $zonesResult[] = array(
                    'objectId' => $hotel->getZone()->getId(),
                    'name' => $hotel->getZone()->getName(),
                    'description' => $hotel->getZone()->getDescription(),
                    'code' => $hotel->getZone()->getCode()
                );
            }

        }

        $response["CITIES"] = $citiesResult;
        $response["ZONES"] = $zonesResult;
        $response["HOTELS"] = $hotelsResult;

        return $response;
    }
}
<?php

namespace ApiBundle\Service;

class FlightCar
{
    private $container = null;

    public function __construct($container = null)
    {
        $this->container = $container;
    }

    public function explore($query)
    {
        $finder = $this->container->get('fos_elastica.finder.app.city');
        $cities = $finder->find($query);

        foreach ($cities as $city)
        {
            $r = array(
                'objectId' => $city->getId(),
                'name' => $city->getName(),
                'iata' => $city->getIata(),
                'label' => $city->getNameLabel()
            );
            $response[] = $r;
        }

        return $response;
    }
}
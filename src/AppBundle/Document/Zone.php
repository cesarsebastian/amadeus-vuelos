<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="zone")
 */
class Zone
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Int()
     */
    private $id_zone_old;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\Int()
     */
    private $code;

    /**
     * @MongoDB\String()
     */
    private $description;

    /**
     * @MongoDB\String()
     */
    private $lat;

    /**
     * @MongoDB\String()
     */
    private $lon;

    /**
     * @MongoDB\Int()
     */
    private $weight;

    /**
     * @MongoDB\Bool
     */
    private $enable;

    /**
     * @MongoDB\ReferenceOne(targetDocument="City")
     */
    private $city;

    /**
     * @MongoDB\String()
     */
    private $airports_recomended;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Airport")
     */
    private $airport;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }
    

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idZoneOld
     *
     * @param int $idZoneOld
     * @return $this
     */
    public function setIdZoneOld($idZoneOld)
    {
        $this->id_zone_old = $idZoneOld;
        return $this;
    }

    /**
     * Get idZoneOld
     *
     * @return int $idZoneOld
     */
    public function getIdZoneOld()
    {
        return $this->id_zone_old;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return int $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * Get lon
     *
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set weight
     *
     * @param int $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * Get weight
     *
     * @return int $weight
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set enable
     *
     * @param bool $enable
     * @return $this
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
        return $this;
    }

    /**
     * Get enable
     *
     * @return bool $enable
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set city
     *
     * @param AppBundle\Document\City $city
     * @return $this
     */
    public function setCity(\AppBundle\Document\City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return AppBundle\Document\City $city
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set airportsRecomended
     *
     * @param string $airportsRecomended
     * @return $this
     */
    public function setAirportsRecomended($airportsRecomended)
    {
        $this->airports_recomended = $airportsRecomended;
        return $this;
    }

    /**
     * Get airportsRecomended
     *
     * @return string $airportsRecomended
     */
    public function getAirportsRecomended()
    {
        return $this->airports_recomended;
    }

    /**
     * Set airport
     *
     * @param AppBundle\Document\Airport $airport
     * @return $this
     */
    public function setAirport(\AppBundle\Document\Airport $airport)
    {
        $this->airport = $airport;
        return $this;
    }

    /**
     * Get airport
     *
     * @return AppBundle\Document\Airport $airport
     */
    public function getAirport()
    {
        return $this->airport;
    }
}

<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="parameter")
 */
class Parameter
{
	/**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\String()
     */
    private $value;  

    /**
     * @MongoDB\ReferenceOne(targetDocument="Client")
     */
    private $client;
  
    public function __construct()
    {
        
    }  

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * @return mixed 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    } 

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    public function __toString()
    {
    	return $this->getName();
    }
}

<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="city")
 */
class City
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\String()
     */
    private $name_from_hotel;

    /**
     * @MongoDB\String()
     */
    private $name_from_aereo;

    /**
     * @MongoDB\String()
     */
    private $name_label;

    /**
     * @MongoDB\String()
     */
    private $iata;

    /**
     * @MongoDB\String()
     */
    private $iata_from_aereo;

    /**
     * @MongoDB\String()
     */
    private $iata_from_hotel;

    /**
     * @MongoDB\String()
     */
    private $lat;

    /**
     * @MongoDB\String()
     */
    private $lon;

    /**
     * @MongoDB\String()
     */
    private $div_admin;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Airport", cascade="persist")
     */
    private $airports;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Country")
     */
    private $country;

    /**
     * @MongoDB\Int()
     */
    private $id_city_old;

    public function __construct()
    {
        $this->airports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name . '-' . $this->iata ;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameFromHotel
     *
     * @param string $nameFromHotel
     * @return $this
     */
    public function setNameFromHotel($nameFromHotel)
    {
        $this->name_from_hotel = $nameFromHotel;
        return $this;
    }

    /**
     * Get nameFromHotel
     *
     * @return string $nameFromHotel
     */
    public function getNameFromHotel()
    {
        return $this->name_from_hotel;
    }

    /**
     * Set nameFromAereo
     *
     * @param string $nameFromAereo
     * @return $this
     */
    public function setNameFromAereo($nameFromAereo)
    {
        $this->name_from_aereo = $nameFromAereo;
        return $this;
    }

    /**
     * Get nameFromAereo
     *
     * @return string $nameFromAereo
     */
    public function getNameFromAereo()
    {
        return $this->name_from_aereo;
    }

    /**
     * Set nameLabel
     *
     * @param string $nameLabel
     * @return $this
     */
    public function setNameLabel($nameLabel)
    {
        $this->name_label = $nameLabel;
        return $this;
    }

    /**
     * Get nameLabel
     *
     * @return string $nameLabel
     */
    public function getNameLabel()
    {
        return $this->name_label;
    }

    /**
     * Set iata
     *
     * @param string $iata
     * @return $this
     */
    public function setIata($iata)
    {
        $this->iata = $iata;
        return $this;
    }

    /**
     * Get iata
     *
     * @return string $iata
     */
    public function getIata()
    {
        return $this->iata;
    }

    /**
     * Set iataFromAereo
     *
     * @param string $iataFromAereo
     * @return $this
     */
    public function setIataFromAereo($iataFromAereo)
    {
        $this->iata_from_aereo = $iataFromAereo;
        return $this;
    }

    /**
     * Get iataFromAereo
     *
     * @return string $iataFromAereo
     */
    public function getIataFromAereo()
    {
        return $this->iata_from_aereo;
    }

    /**
     * Set iataFromHotel
     *
     * @param string $iataFromHotel
     * @return $this
     */
    public function setIataFromHotel($iataFromHotel)
    {
        $this->iata_from_hotel = $iataFromHotel;
        return $this;
    }

    /**
     * Get iataFromHotel
     *
     * @return string $iataFromHotel
     */
    public function getIataFromHotel()
    {
        return $this->iata_from_hotel;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * Get lon
     *
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set divAdmin
     *
     * @param string $divAdmin
     * @return $this
     */
    public function setDivAdmin($divAdmin)
    {
        $this->div_admin = $divAdmin;
        return $this;
    }

    /**
     * Get divAdmin
     *
     * @return string $divAdmin
     */
    public function getDivAdmin()
    {
        return $this->div_admin;
    }

    /**
     * Add airport
     *
     * @param AppBundle\Document\Airport $airport
     */
    public function addAirport(\AppBundle\Document\Airport $airport)
    {
        $this->airports[] = $airport;
    }

    /**
     * Remove airport
     *
     * @param AppBundle\Document\Airport $airport
     */
    public function removeAirport(\AppBundle\Document\Airport $airport)
    {
        $this->airports->removeElement($airport);
    }

    /**
     * Get airports
     *
     * @return \Doctrine\Common\Collections\Collection $airports
     */
    public function getAirports()
    {
        return $this->airports;
    }

    /**
     * Set country
     *
     * @param AppBundle\Document\Country $country
     * @return $this
     */
    public function setCountry(\AppBundle\Document\Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return AppBundle\Document\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set idCityOld
     *
     * @param int $idCityOld
     * @return $this
     */
    public function setIdCityOld($idCityOld)
    {
        $this->id_city_old = $idCityOld;
        return $this;
    }

    /**
     * Get idCityOld
     *
     * @return int $idCityOld
     */
    public function getIdCityOld()
    {
        return $this->id_city_old;
    }
}

<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="hotel")
 */
class Hotel
{
	/**
     * @MongoDB\Id
     */
    private $id;

     /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\String()
     */
    private $direccion;

    /**
     * @MongoDB\String()
     */
    private $lat;

    /**
     * @MongoDB\String()
     */
    private $lon;

    /**
     * @MongoDB\String()
     */
    private $giata;

    /**
     * @MongoDB\Int()
     */
    private $category;

    /**
     * @MongoDB\Int()
     */
    private $code;

    /**
     * @MongoDB\Int()
     */
    private $proveedor;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Zone")
     */
    private $zone;

    /**
     * @MongoDB\ReferenceOne(targetDocument="City")
     */
    private $city;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Country")
     */
    private $country;

    /**
     * @MongoDB\Int()
     */
    private $cod_zone;

    /**
     * @MongoDB\Int()
     */
    private $cod_city;

    /**
     * @MongoDB\Int()
     */
    private $cod_country;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->name ;
    }
    


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return $this
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string $direccion
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * Get lon
     *
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set giata
     *
     * @param string $giata
     * @return $this
     */
    public function setGiata($giata)
    {
        $this->giata = $giata;
        return $this;
    }

    /**
     * Get giata
     *
     * @return string $giata
     */
    public function getGiata()
    {
        return $this->giata;
    }

    /**
     * Set category
     *
     * @param int $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     *
     * @return int $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return int $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set proveedor
     *
     * @param int $proveedor
     * @return $this
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;
        return $this;
    }

    /**
     * Get proveedor
     *
     * @return int $proveedor
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set zone
     *
     * @param AppBundle\Document\Zone $zone
     * @return $this
     */
    public function setZone(\AppBundle\Document\Zone $zone)
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * Get zone
     *
     * @return AppBundle\Document\Zone $zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set city
     *
     * @param AppBundle\Document\City $city
     * @return $this
     */
    public function setCity(\AppBundle\Document\City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return AppBundle\Document\City $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param AppBundle\Document\Country $country
     * @return $this
     */
    public function setCountry(\AppBundle\Document\Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return AppBundle\Document\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set codZone
     *
     * @param int $codZone
     * @return $this
     */
    public function setCodZone($codZone)
    {
        $this->cod_zone = $codZone;
        return $this;
    }

    /**
     * Get codZone
     *
     * @return int $codZone
     */
    public function getCodZone()
    {
        return $this->cod_zone;
    }

    /**
     * Set codCity
     *
     * @param int $codCity
     * @return $this
     */
    public function setCodCity($codCity)
    {
        $this->cod_city = $codCity;
        return $this;
    }

    /**
     * Get codCity
     *
     * @return int $codCity
     */
    public function getCodCity()
    {
        return $this->cod_city;
    }

    /**
     * Set codCountry
     *
     * @param int $codCountry
     * @return $this
     */
    public function setCodCountry($codCountry)
    {
        $this->cod_country = $codCountry;
        return $this;
    }

    /**
     * Get codCountry
     *
     * @return int $codCountry
     */
    public function getCodCountry()
    {
        return $this->cod_country;
    }
}

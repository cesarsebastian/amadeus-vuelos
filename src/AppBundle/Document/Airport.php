<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="airport")
 */
class Airport
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\String()
     */
    private $lat;

    /**
     * @MongoDB\String()
     */
    private $lon;

    /**
     * @MongoDB\String()
     */
    private $iata;

    /**
     * @MongoDB\String()
     */
    private $geo_point_oid;

    /**
     * @MongoDB\String()
     */
    private $geo_area_oid;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set namePlain
     *
     * @param string $namePlain
     * @return $this
     */
    public function setNamePlain($namePlain)
    {
        $this->name_plain = $namePlain;
        return $this;
    }

    /**
     * Get namePlain
     *
     * @return string $namePlain
     */
    public function getNamePlain()
    {
        return $this->name_plain;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * Get lat
     *
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * Get lon
     *
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set iata
     *
     * @param string $iata
     * @return $this
     */
    public function setIata($iata)
    {
        $this->iata = $iata;
        return $this;
    }

    /**
     * Get iata
     *
     * @return string $iata
     */
    public function getIata()
    {
        return $this->iata;
    }

    /**
     * Set geoPointOid
     *
     * @param string $geoPointOid
     * @return $this
     */
    public function setGeoPointOid($geoPointOid)
    {
        $this->geo_point_oid = $geoPointOid;
        return $this;
    }

    /**
     * Get geoPointOid
     *
     * @return string $geoPointOid
     */
    public function getGeoPointOid()
    {
        return $this->geo_point_oid;
    }

    /**
     * Set geoAreaOid
     *
     * @param string $geoAreaOid
     * @return $this
     */
    public function setGeoAreaOid($geoAreaOid)
    {
        $this->geo_area_oid = $geoAreaOid;
        return $this;
    }

    /**
     * Get geoAreaOid
     *
     * @return string $geoAreaOid
     */
    public function getGeoAreaOid()
    {
        return $this->geo_area_oid;
    }
}

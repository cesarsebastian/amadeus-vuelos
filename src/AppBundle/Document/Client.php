<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @MongoDB\Document(collection="client")
 * @Vich\Uploadable
 */
class Client
{
	/**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @var \DateTime
     * @MongoDB\Timestamp
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @MongoDB\Timestamp
     */
    protected $updatedAt;

    /**
     * @MongoDB\Boolean()
     */
    private $production_mode;

    /**
     * @MongoDB\String()
     */
    private $callback;

    /**
     * @MongoDB\String()
     */
    private $origin_url;

    /**
     * @MongoDB\String()
     */
    private $checkoutidentifier;

    /**
     * @MongoDB\String()
     */
    private $keys_sandbox_consumerkey;

    /**
     * @MongoDB\String()
     */
    private $keys_sandbox_keystorepath;

    /**
     * @MongoDB\String()
     */
    private $keys_sandbox_keystorepassword;

    /**
     * @MongoDB\String()
     */
    private $keys_prod_consumerkey;

    /**
     * @MongoDB\String()
     */
    private $keys_prod_keystorepath;

    /**
     * @MongoDB\String()
     */
    private $keys_prod_keystorepassword;

    /**
     * @MongoDB\String()
     */
    private $token_sandbox;

    /**
     * @MongoDB\String()
     */
    private $token_production;

    public function __construct()
    {
        
    }

    public function __toString()
    {
        if (!$this->name)
            return '';
        else
            return $this->name;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param timestamp $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return timestamp $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param timestamp $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return timestamp $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set productionMode
     *
     * @param boolean $productionMode
     * @return $this
     */
    public function setProductionMode($productionMode)
    {
        $this->production_mode = $productionMode;
        return $this;
    }

    /**
     * Get productionMode
     *
     * @return boolean $productionMode
     */
    public function getProductionMode()
    {
        return $this->production_mode;
    }

    /**
     * Set callback
     *
     * @param string $callback
     * @return $this
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * Get callback
     *
     * @return string $callback
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * Set originUrl
     *
     * @param string $originUrl
     * @return $this
     */
    public function setOriginUrl($originUrl)
    {
        $this->origin_url = $originUrl;
        return $this;
    }

    /**
     * Get originUrl
     *
     * @return string $originUrl
     */
    public function getOriginUrl()
    {
        return $this->origin_url;
    }

    /**
     * Set checkoutidentifier
     *
     * @param string $checkoutidentifier
     * @return $this
     */
    public function setCheckoutidentifier($checkoutidentifier)
    {
        $this->checkoutidentifier = $checkoutidentifier;
        return $this;
    }

    /**
     * Get checkoutidentifier
     *
     * @return string $checkoutidentifier
     */
    public function getCheckoutidentifier()
    {
        return $this->checkoutidentifier;
    }

    /**
     * Set keysSandboxConsumerkey
     *
     * @param string $keysSandboxConsumerkey
     * @return $this
     */
    public function setKeysSandboxConsumerkey($keysSandboxConsumerkey)
    {
        $this->keys_sandbox_consumerkey = $keysSandboxConsumerkey;
        return $this;
    }

    /**
     * Get keysSandboxConsumerkey
     *
     * @return string $keysSandboxConsumerkey
     */
    public function getKeysSandboxConsumerkey()
    {
        return $this->keys_sandbox_consumerkey;
    }

    /**
     * Set keysSandboxKeystorepath
     *
     * @param string $keysSandboxKeystorepath
     * @return $this
     */
    public function setKeysSandboxKeystorepath($keysSandboxKeystorepath)
    {
        $this->keys_sandbox_keystorepath = $keysSandboxKeystorepath;
        return $this;
    }

    /**
     * Get keysSandboxKeystorepath
     *
     * @return string $keysSandboxKeystorepath
     */
    public function getKeysSandboxKeystorepath()
    {
        return $this->keys_sandbox_keystorepath;
    }

    /**
     * Set keysSandboxKeystorepassword
     *
     * @param string $keysSandboxKeystorepassword
     * @return $this
     */
    public function setKeysSandboxKeystorepassword($keysSandboxKeystorepassword)
    {
        $this->keys_sandbox_keystorepassword = $keysSandboxKeystorepassword;
        return $this;
    }

    /**
     * Get keysSandboxKeystorepassword
     *
     * @return string $keysSandboxKeystorepassword
     */
    public function getKeysSandboxKeystorepassword()
    {
        return $this->keys_sandbox_keystorepassword;
    }

    /**
     * Set keysProdConsumerkey
     *
     * @param string $keysProdConsumerkey
     * @return $this
     */
    public function setKeysProdConsumerkey($keysProdConsumerkey)
    {
        $this->keys_prod_consumerkey = $keysProdConsumerkey;
        return $this;
    }

    /**
     * Get keysProdConsumerkey
     *
     * @return string $keysProdConsumerkey
     */
    public function getKeysProdConsumerkey()
    {
        return $this->keys_prod_consumerkey;
    }

    /**
     * Set keysProdKeystorepath
     *
     * @param string $keysProdKeystorepath
     * @return $this
     */
    public function setKeysProdKeystorepath($keysProdKeystorepath)
    {
        $this->keys_prod_keystorepath = $keysProdKeystorepath;
        return $this;
    }

    /**
     * Get keysProdKeystorepath
     *
     * @return string $keysProdKeystorepath
     */
    public function getKeysProdKeystorepath()
    {
        return $this->keys_prod_keystorepath;
    }

    /**
     * Set keysProdKeystorepassword
     *
     * @param string $keysProdKeystorepassword
     * @return $this
     */
    public function setKeysProdKeystorepassword($keysProdKeystorepassword)
    {
        $this->keys_prod_keystorepassword = $keysProdKeystorepassword;
        return $this;
    }

    /**
     * Get keysProdKeystorepassword
     *
     * @return string $keysProdKeystorepassword
     */
    public function getKeysProdKeystorepassword()
    {
        return $this->keys_prod_keystorepassword;
    }

    /**
     * Set tokenSandbox
     *
     * @param string $tokenSandbox
     * @return $this
     */
    public function setTokenSandbox($tokenSandbox)
    {
        $this->token_sandbox = $tokenSandbox;
        return $this;
    }

    /**
     * Get tokenSandbox
     *
     * @return string $tokenSandbox
     */
    public function getTokenSandbox()
    {
        return $this->token_sandbox;
    }

    /**
     * Set tokenProduction
     *
     * @param string $tokenProduction
     * @return $this
     */
    public function setTokenProduction($tokenProduction)
    {
        $this->token_production = $tokenProduction;
        return $this;
    }

    /**
     * Get tokenProduction
     *
     * @return string $tokenProduction
     */
    public function getTokenProduction()
    {
        return $this->token_production;
    }
}

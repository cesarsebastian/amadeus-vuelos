<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="log")
 */
class Log
{
	/**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Date()
     */
    private $date;

    /**
     * @MongoDB\String()
     */
    private $origin;

    /**
     * @MongoDB\String()
     */
    private $url;

    /**
     * @MongoDB\String()
     */
    private $request;

    /**
     * @MongoDB\String()
     */
    private $response;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Token")
     */
    private $token;

    public function __construct()
    {
        
    }  

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }   
    
    /**
     * @return mixed
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @return mixed $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function __toString()
    {
       return  $this->getOrigin() . $this->getDate();
    }
}

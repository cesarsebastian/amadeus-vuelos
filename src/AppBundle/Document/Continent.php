<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="continent")
 */
class Continent
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String()
     */
    protected $name;

    /**
     * @MongoDB\String()
     */
    private $name_plain;

    /**
     * @MongoDB\Int()
     */
    private $id_aereo;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->name ;
    }


    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set namePlain
     *
     * @param string $namePlain
     * @return $this
     */
    public function setNamePlain($namePlain)
    {
        $this->name_plain = $namePlain;
        return $this;
    }

    /**
     * Get namePlain
     *
     * @return string $namePlain
     */
    public function getNamePlain()
    {
        return $this->name_plain;
    }

    /**
     * Set idAereo
     *
     * @param int $idAereo
     * @return $this
     */
    public function setIdAereo($idAereo)
    {
        $this->id_aereo = $idAereo;
        return $this;
    }

    /**
     * Get idAereo
     *
     * @return int $idAereo
     */
    public function getIdAereo()
    {
        return $this->id_aereo;
    }
}

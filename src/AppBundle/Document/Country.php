<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="country")
 */
class Country
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String()
     */
    private $name;

    /**
     * @MongoDB\String()
     */
    private $name_formal;

    /**
     * @MongoDB\String()
     */
    private $code_iso_two;

    /**
     * @MongoDB\String()
     */
    private $code_iso_three;

    /**
     * @MongoDB\String()
     */
    private $name_capital;

    /**
     * @MongoDB\String()
     */
    private $type;

    /**
     * @MongoDB\String()
     */
    private $code_iso_currency;

    /**
     * @MongoDB\String()
     */
    private $code_name_currency;

    /**
     * @MongoDB\String()
     */
    private $code_telephone;

    /**
     * @MongoDB\String()
     */
    private $code_domain;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Continent")
     */
    private $continent;

    /**
     * @MongoDB\Int()
     */
    private $id_aereo;

    /**
     * @MongoDB\Int()
     */
    private $id_hotel;

    /**
     * @MongoDB\Int()
     */
    private $id_continent;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->name ;
    }


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameFormal
     *
     * @param string $nameFormal
     * @return $this
     */
    public function setNameFormal($nameFormal)
    {
        $this->name_formal = $nameFormal;
        return $this;
    }

    /**
     * Get nameFormal
     *
     * @return string $nameFormal
     */
    public function getNameFormal()
    {
        return $this->name_formal;
    }

    /**
     * Set codeIsoTwo
     *
     * @param string $codeIsoTwo
     * @return $this
     */
    public function setCodeIsoTwo($codeIsoTwo)
    {
        $this->code_iso_two = $codeIsoTwo;
        return $this;
    }

    /**
     * Get codeIsoTwo
     *
     * @return string $codeIsoTwo
     */
    public function getCodeIsoTwo()
    {
        return $this->code_iso_two;
    }

    /**
     * Set codeIsoThree
     *
     * @param string $codeIsoThree
     * @return $this
     */
    public function setCodeIsoThree($codeIsoThree)
    {
        $this->code_iso_three = $codeIsoThree;
        return $this;
    }

    /**
     * Get codeIsoThree
     *
     * @return string $codeIsoThree
     */
    public function getCodeIsoThree()
    {
        return $this->code_iso_three;
    }

    /**
     * Set nameCapital
     *
     * @param string $nameCapital
     * @return $this
     */
    public function setNameCapital($nameCapital)
    {
        $this->name_capital = $nameCapital;
        return $this;
    }

    /**
     * Get nameCapital
     *
     * @return string $nameCapital
     */
    public function getNameCapital()
    {
        return $this->name_capital;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set codeIsoCurrency
     *
     * @param string $codeIsoCurrency
     * @return $this
     */
    public function setCodeIsoCurrency($codeIsoCurrency)
    {
        $this->code_iso_currency = $codeIsoCurrency;
        return $this;
    }

    /**
     * Get codeIsoCurrency
     *
     * @return string $codeIsoCurrency
     */
    public function getCodeIsoCurrency()
    {
        return $this->code_iso_currency;
    }

    /**
     * Set codeNameCurrency
     *
     * @param string $codeNameCurrency
     * @return $this
     */
    public function setCodeNameCurrency($codeNameCurrency)
    {
        $this->code_name_currency = $codeNameCurrency;
        return $this;
    }

    /**
     * Get codeNameCurrency
     *
     * @return string $codeNameCurrency
     */
    public function getCodeNameCurrency()
    {
        return $this->code_name_currency;
    }

    /**
     * Set codeTelephone
     *
     * @param string $codeTelephone
     * @return $this
     */
    public function setCodeTelephone($codeTelephone)
    {
        $this->code_telephone = $codeTelephone;
        return $this;
    }

    /**
     * Get codeTelephone
     *
     * @return string $codeTelephone
     */
    public function getCodeTelephone()
    {
        return $this->code_telephone;
    }

    /**
     * Set codeDomain
     *
     * @param string $codeDomain
     * @return $this
     */
    public function setCodeDomain($codeDomain)
    {
        $this->code_domain = $codeDomain;
        return $this;
    }

    /**
     * Get codeDomain
     *
     * @return string $codeDomain
     */
    public function getCodeDomain()
    {
        return $this->code_domain;
    }

    /**
     * Set continent
     *
     * @param AppBundle\Document\Continent $continent
     * @return $this
     */
    public function setContinent(\AppBundle\Document\Continent $continent)
    {
        $this->continent = $continent;
        return $this;
    }

    /**
     * Get continent
     *
     * @return AppBundle\Document\Continent $continent
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set idAereo
     *
     * @param int $idAereo
     * @return $this
     */
    public function setIdAereo($idAereo)
    {
        $this->id_aereo = $idAereo;
        return $this;
    }

    /**
     * Get idAereo
     *
     * @return int $idAereo
     */
    public function getIdAereo()
    {
        return $this->id_aereo;
    }

    /**
     * Set idHotel
     *
     * @param int $idHotel
     * @return $this
     */
    public function setIdHotel($idHotel)
    {
        $this->id_hotel = $idHotel;
        return $this;
    }

    /**
     * Get idHotel
     *
     * @return int $idHotel
     */
    public function getIdHotel()
    {
        return $this->id_hotel;
    }

    /**
     * Set idContinent
     *
     * @param int $idContinent
     * @return $this
     */
    public function setIdContinent($idContinent)
    {
        $this->id_continent = $idContinent;
        return $this;
    }

    /**
     * Get idContinent
     *
     * @return int $idContinent
     */
    public function getIdContinent()
    {
        return $this->id_continent;
    }
}

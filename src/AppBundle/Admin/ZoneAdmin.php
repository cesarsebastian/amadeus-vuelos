<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ZoneAdmin extends Admin
{
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->add('name',NULL, array(
               'required' => true,
               'label' => 'Name'))
			->add('code', NULL, array(
               'required' => true,
               'label' => 'Code'))
			->add('description', NULL, array(
               'label' => 'Description'))
			->add('lat', NULL, array(
               'label' => 'Latitude'))
            ->add('lon', NULL, array(
                'label' => 'Longitude'))
            ->add('weight', NULL, array(
               'label' => 'Weight'))
            ->add('enable', NULL, array(
               'label' => 'Enable'))
            ->add(
                'city',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\City',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )
            ->add('airports_recomended')
            //->add('airport')
            ->add(
                'airport',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\Airport',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )

        ;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
            ->add('name')
            ->add('code')
            ->add('description')
            ->add('lat')
            ->add('lon')
            ->add('weight')
            ->add('enable')
            //->add('city')
            //->add('airport')

        ;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
            ->addIdentifier('name')
            ->add('code')
            ->add('description')
            ->add('lat')
            ->add('lon')
            ->add('weight')
            ->add('enable')
            ->add('city')
            ->add('airport')
            ->add('airports_recomended')
        ;
	}
}
<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ClientAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Datos de cliente')
                ->with('Básicos')
                    ->add('name')
                ->end()
            ->end()
            ->tab('Master Pass')
                ->with('Datos Básicos')
                    ->add('callback')
                    ->add('origin_url')
                    ->add('checkoutidentifier')
                    ->add('production_mode', null, array('required' => false))
                ->end()
                ->with('Datos Sandbox', array('class' => 'col-md-6'))
                    ->add('keys_sandbox_consumerkey')
                    ->add('keys_sandbox_keystorepath', 'file', array('label' => 'Image file', 'required' => true))
                    ->add('keys_sandbox_keystorepassword')
                    ->add('token_sandbox')
                    /*->add('token_sandbox', 'sonata_type_model_list',array(), array(
                        'edit' => 'inline',
                        'inline' => 'standard',
                    ))
                    ->add(
                        'token_sandbox',
                        'sonata_type_model_list',
                        array(
                            'btn_add'       => 'Nuevo Token',      //Specify a custom label
                            'btn_list'      => false,     //which will be translated
                            'btn_delete'    => false
                        ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'standard',
                        )
                    )*/
                ->end()
                ->with('Datos Producción', array('class' => 'col-md-6'))
                    ->add('keys_prod_consumerkey', null, array('required' => false))
                    ->add('keys_prod_keystorepath', 'file', array('label' => 'Image file', 'required' => false))
                    ->add('keys_prod_keystorepassword', null, array('required' => false))
                    ->add('token_production', null, array('required' => false))
                    /*->add(
                        'token_production',
                        'sonata_type_model_list',
                        array(
                            'btn_add'       => 'Nuevo Token',      //Specify a custom label
                            'btn_list'      => false,     //which will be translated
                            'btn_delete'    => false,
                            'required'      => false,
                        ),
                        array(
                            'edit' => 'inline',
                            'inline' => 'standard',
                        )
                    )*/
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('name')
            ->add('production_mode')
            ->add('callback')
            ->add('origin_url')
            ->add('checkoutidentifier')
//            ->add('keys_sandbox_consumerkey')
//            ->add('keys_sandbox_keystorepath')
//            ->add('keys_sandbox_keystorepassword')
//            ->add('keys_prod_consumerkey')
//            ->add('keys_prod_keystorepath')
//            ->add('keys_prod_keystorepassword')
        ;
    }

    public function preUpdate($client)
    {
        $this->callServiceUpload($client);
    }

    public function prePersist($client)
    {
        $this->callServiceUpload($client);
    }

    private function callServiceUpload($client)
    {
        $file = $client->getImageFile();
        if($file)
        {
            $serviceUpLoadRoute = $this->getConfigurationPool()->getContainer()->get('app.uploadroute');
            $serviceUpLoadRoute->upload($file,$client);
        }
    }

}
<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CountryAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        	->add('name',NULL, array(
                'required' => true,
                'label' => 'Name'))
            ->add('name_formal',NULL, array(
                'label' => 'Name formal'))
            ->add('code_iso_two',NULL, array(
                'label' => 'ISO (2)'))
            ->add('code_iso_three',NULL, array(
                'label' => 'ISO (3)'))
            ->add('name_capital',NULL, array(
                'label' => 'Name capital'))
            ->add('type',NULL, array(
                'label' => 'Name formal'))
            ->add('code_iso_currency',NULL, array(
                'label' => 'Currency ISO'))
            ->add('code_name_currency',NULL, array(
                'required' => false,
                'label' => 'Currency name'))
            ->add('code_telephone',NULL, array(
                'required' => false,
                'label' => 'Code telephone'))
            ->add('code_domain',NULL, array(
                'required' => false,
                'label' => 'Code domain'))
            //->add('continent')
            ->add(
                'continent',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\Continent',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('name_formal')
            ->add('code_iso_two')
            ->add('code_iso_three')
            ->add('name_capital')
            ->add('type')
            ->add('code_iso_currency')
            ->add('code_name_currency')
            ->add('code_telephone')
            ->add('code_domain')
            ->add('continent')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('name_formal')
            ->add('code_iso_two')
            ->add('code_iso_three')
            ->add('name_capital')
            ->add('type')
            ->add('code_iso_currency')
            ->add('code_name_currency')
            ->add('code_telephone')
            ->add('code_domain')
            ->add('continent')
        ;
    }
}
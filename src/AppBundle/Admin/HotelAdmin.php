<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class HotelAdmin extends Admin
{
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->add('name',NULL, array(
				'required' => true,
				'label' => 'Name'))
            ->add('direccion',NULL, array(
				'label' => 'Direction'))
            ->add('lat',NULL, array(
				'label' => 'Latitude'))
            ->add('lon',NULL, array(
                'label' => 'Longitude'))
            ->add('category',NULL, array(
                'label' => 'Category'))
            ->add('proveedor',NULL, array(
                'label' => 'Provider'))
            ->add('code',NULL, array(
                'label' => 'Code'))
            ->add(
                'zone',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\Zone',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )
            ->add(
                'city',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\City',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )
            ->add(
                'country',
                'sonata_type_model_autocomplete',
                [
                    'class' => 'AppBundle\Document\Country',
                    'property' => 'name',
                    'model_manager' => $this->modelManager,
                ],
                [
                    'type_options' =>
                        ['delete' => false]
                ]
            )
            //->add('zone')
            /*->add('city', 'sonata_type_model_autocomplete', array(
                'property' => 'name',
                'multiple' => true,
                'required' => false
                ))*/
            //->add('city')
            //->add('country')
        ;

	}

    protected function getObjectPropertyValue($object, $property)
    {
        if (!$this->propertyAccessor) {
            $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->propertyAccessor->getValue($object, $property);
    }

	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
            ->add('name')
            ->add('direccion')
            ->add('lat')
            ->add('lon')
            ->add('category')
            ->add('proveedor')
            ->add('code')
        ;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
            ->addIdentifier('name')
            ->add('direccion')
            ->add('lat')
            ->add('lon')
            ->add('category')
            ->add('proveedor')
            ->add('code')
            ->add('zone')
            ->add('city')
            ->add('country')
		;
		
	}
}
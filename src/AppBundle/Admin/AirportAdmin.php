<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AirportAdmin extends Admin
{
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->add('name',NULL, array(
               'required' => true,
               'label' => 'Name'))
			->add('lat', NULL, array(
               'required' => true,
               'label' => 'Latitude'))
			->add('lon', NULL, array(
               'required' => true,
               'label' => 'Longitude'))
			->add('iata', NULL, array(
               'required' => true,
               'label' => 'IATA'))
            ->add('geo_point_oid', NULL, array(
               'label' => 'Geo Point'))
            ->add('geo_area_oid', NULL, array(
               'label' => 'Geo Area'))
			;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
            ->add('name')
            ->add('iata')
            ->add('geo_point_oid')
            ->add('geo_area_oid')
        ;
	}

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('name');
		$listMapper->add('lat');
		$listMapper->add('lon');
		$listMapper->add('iata');
		$listMapper->add('geo_point_oid');
		$listMapper->add('geo_area_oid');
	}
}
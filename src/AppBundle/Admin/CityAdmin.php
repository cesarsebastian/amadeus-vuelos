<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CityAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('City')
                ->with('General', array('class' => 'col-md-6'))

                    ->add('iata',NULL, array(
                        'required' => true,
                        'label' => 'IATA'))
                    ->add('iata_from_hotel',NULL, array(
                        'label' => 'IATA - Hotel'))
                    ->add('iata_from_aereo',NULL, array(
                        'label' => 'IATA - Aereo'))
                	->add('name',NULL, array(
                       'required' => true,
                       'label' => 'Name'))
                    ->add('name_from_hotel',NULL, array(
                        'label' => 'Name - Hotel'))
                    ->add('name_from_aereo',NULL, array(
                        'label' => 'Name - Aereo'))
                    ->add('name_label',NULL, array(
                        'required' => true,
                        'label' => 'Name label'))
                    ->add('lat', NULL, array(
                        'label' => 'Latitude'))
                    ->add('lon',NULL, array(
                        'label' => 'Longitude'))
                    ->add('div_admin',NULL, array(
                       'label' => 'División admin'))
                    ->add('country')
                ->end()
            ->end()
            ->tab('Airports')
                ->with('List of airports', array('class' => 'col-md-6'))
                    ->add('airports', 'sonata_type_collection', array(), array(
                        'edit' => 'inline',
                        'inline' => 'table'
                    ))
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('iata')
            ->add('iata_from_hotel')
            ->add('iata_from_aereo')
            ->add('name')
            ->add('name_from_hotel')
            ->add('name_from_aereo')
            ->add('name_label')
            ->add('country')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name', NULL, array(), array('editable' => true))
            ->add('iata', 'sonata_type_admin', array(), array('edit' => 'inline'))
            ->add('iata_from_hotel', NULL, array(), array('edit' => 'inline'))
            ->add('iata_from_aereo')
            ->add('name_from_hotel')
            ->add('name_from_aereo')
            ->add('name_label')
            ->add('country')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }
}
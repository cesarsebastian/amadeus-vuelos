<?php

namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\Document\Airport;
use AppBundle\Document\City;
use AppBundle\Document\Country;
use AppBundle\Document\Hotel;
use AppBundle\Document\Zone;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Document\Continent;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;

class MigrateData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $multiproveedor;
    private $aereo;
    private $manager;

    private $limit = ' ';

    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;

        $this->multiproveedor = $container->get('doctrine.dbal.multiproveedor_connection');

        $this->aereo = $container->get('doctrine.dbal.aereo_connection');

        $this->manager = $container->get('doctrine_mongodb')->getManager();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /*var_dump('init: migracionContinenteAereos');
        $this->migracionContinenteAereos();
        var_dump('init: migracionPaisesAereos');
        $this->migracionPaisesAereos();
        var_dump('init: migracionPaisesHoteles');
        $this->migracionPaisesHoteles();
        var_dump('init: migracionCiudadesHoteles');
        $this->migracionCiudadesHoteles();
        var_dump('init: migracionCiudadesAereos');
        $this->migracionCiudadesAereos();
        var_dump('init: migracionContienentePais');
        $this->migracionContienentePais();
        var_dump('init: migracionAeropuertos');
        $this->migracionAeropuertos();
        var_dump('init: migracionZonas');
        $this->migracionZonas();
        var_dump('init: migracionHoteles');
        $this->migracionHoteles();
        var_dump('init: migracionHoteles relaciones');
        //$this->migracionHotelesRelaciones();
        var_dump('finish');*/
    }

    //1
    public function migracionContinenteAereos()
    {
        $RAW_QUERY = 'SELECT * FROM rollback_tijetravel.continentes;';

        $continentes = $this->aereo->fetchAll($RAW_QUERY);

        foreach($continentes as $continente)
        {
            $continent = new Continent();
            $continent->setIdAereo($continente['id']);
            $continent->setName($continente['continente']);

            $this->manager->persist($continent);
        }

        $this->manager->flush();

    }

    //2
    public function migracionPaisesAereos()
    {
        //TABLA COUNTRY 268
        $RAW_QUERY = 'SELECT * FROM rollback_tijetravel.country;';

        $paises = $this->aereo->fetchAll($RAW_QUERY);

        foreach($paises as $pais)
        {
            $country = new Country();
            $country->setName($pais['common_name']);
            $country->setNameFormal($pais['formal_name']);
            $country->setCodeIsoTwo($pais['ISO_3166_1_2_letter_code']);
            $country->setCodeIsoThree($pais['ISO_3166_1_3_letter_code']);
            $country->setNameCapital($pais['capital']);
            $country->setType($pais['type']);
            $country->setCodeIsoCurrency($pais['ISO_4217_currency_code']);
            $country->setCodeNameCurrency($pais['ISO_4217_currency_name']);
            $country->setCodeTelephone($pais['ITU_T_telephone_code']);
            $country->setCodeDomain($pais['IANA_country_code_TLD']);

            $this->manager->persist($country);
        }

        $this->manager->flush();

        //TABLA PAISES 240
        $RAW_QUERY = 'SELECT * FROM rollback_tijetravel.paises '.$this->limit.';';

        $paises = $this->aereo->fetchAll($RAW_QUERY);

        foreach($paises as $pais)
        {
            $country = $this->manager->getRepository('AppBundle:Country')->findOneBy(array('code_iso_two' => $pais['iata']));

            if(!$country)
            {
                $country = new Country();
                $country->setName($pais['nombre']);
                $country->setCodeIsoTwo($pais['iata']);
                $country->setCodeIsoThree($pais['iso_tres']);
            }

            $country->setIdAereo($pais['id']);

            $this->manager->persist($country);
        }

        $this->manager->flush();
    }

    //3
    public function migracionPaisesHoteles()
    {
        $RAW_QUERY = 'SELECT * FROM multihoteles.countries;';

        $paises = $this->multiproveedor->fetchAll($RAW_QUERY);

        foreach($paises as $pais)
        {
            $country = $this->manager->getRepository('AppBundle:Country')->findOneBy(array('code_iso_two' => $pais['code']));

            if(!$country)
            {
                $country = new Country();
                $country->setName($pais['description']);
                $country->setCodeIsoTwo($pais['code']);
            }

            $country->setIdHotel($pais['id']);

            $this->manager->persist($country);
        }

        $this->manager->flush();

    }

    //4
    public function migracionCiudadesHoteles()
    {
        //4916 en la tabla hoteles_destinations que es ciudade en hoteles
        $RAW_QUERY = 'SELECT * FROM multihoteles.hoteles_destinations;';

        $ciudades = $this->multiproveedor->fetchAll($RAW_QUERY);

        foreach($ciudades as $ciudad)
        {
            $city = $this->manager->getRepository('AppBundle:City')->findOneBy(array('iata' => $ciudad['code']));

            if(!$city)
            {
                $city = new City();
                $city->setIdCityOld($ciudad['id']);
                $city->setName($ciudad['name']);
                $city->setNameFromHotel($ciudad['name']);
                $city->setIata($ciudad['code']);
                $city->setIataFromHotel($ciudad['code']);
                $city->setLat($ciudad['latitud']);
                $city->setLon($ciudad['longitud']);

            }

            $country = $this->manager->getRepository('AppBundle:Country')->findOneBy(array('id_hotel' => intval($ciudad['codeCountry'])));

            if($country)
            {
                $city->setCountry($country);
            }


            //$city->setLabelQuestionHotel($country->getName() + '-' + $ciudad['name']); //nuevo 17/04/2017

            //$city->setIdQuestionHotel($ciudad['id'] + '-' + $ciudad['codeCountry']); //nuevo 17/04/2017


            $this->manager->persist($city);
        }

        $this->manager->flush();
    }

    //5
    public function migracionCiudadesAereos()
    {
        //9524 registros en geocitys, en muchas la ciudad tiene iata del aeropuerto

        $RAW_QUERY = 'SELECT * FROM rollback_tijetravel.geocitys;';
        //$RAW_QUERY = 'SELECT * FROM rollback_tijetravel.geocitys where iata = "BUE";';

        $ciudades = $this->aereo->fetchAll($RAW_QUERY);

        foreach($ciudades as $ciudad)
        {
            $city = $this->manager->getRepository('AppBundle:City')->findOneBy(array('iata' => $ciudad['iata']));

            if($city)
            {
                //existe la ciudad pero nose si tiene el mismo nombre
                if($city->getName() != $ciudad['city'])
                {
                    //tenemos el mismo iata para ciudades que tienen distinto nombre, quizas uno sea del aeropuerto el otro de ciudad
                    //iata vacio = se debe definir por administrador y poner el correspondiente
                    //tambien se deben reubicar los hoteles, aeropuertos y zonas

                    $city->setIata('');
                    $city->setName('');

                    /*
                    //doy de alta una nueva ciudad para que luego lo corrigan ellos a mano cual queda con quien
                    $cityDuplicate = new City();
                    $cityDuplicate->setName(''); //debe decidir cual le queda
                    $cityDuplicate->setIata(''); //debe decidir cual le queda
                    $cityDuplicate->setIataFromAereo($ciudad['iata']);
                    $cityDuplicate->setIataFromHotel($ciudad['iata']);
                    $cityDuplicate->setNameFromHotel($city->getName());
                    //$cityDuplicate->setIataFromHotel(); //por que es igual
                    //$cityDuplicate->setNameFromHotel(); //
                    $cityDuplicate->setDivAdmin($ciudad['adminDiv']);
                    $cityDuplicate->setNameLabel($ciudad['label']);


                    $this->manager->persist($cityDuplicate);
                    */


                }

                $city->setIataFromAereo($ciudad['iata']);
                $city->setNameFromAereo($ciudad['city']);

                //aqui hay que agregarle el label
                $city->setNameLabel($ciudad['label']);

            } else {
                $city = new City();
                $city->setName($ciudad['city']);
                $city->setIata($ciudad['iata']);
                $city->setIataFromAereo($ciudad['iata']);
                $city->setDivAdmin($ciudad['adminDiv']);
                $city->setNameLabel($ciudad['label']);
            }

            if(!$city->getCountry())
            {
                //busco a que pais pertenece la ciudad
                $country = $this->manager->getRepository('AppBundle:Country')->findOneBy(array('name' => $ciudad['country']));

                if ($country)
                {
                    $country->setIdContinent(intval($ciudad['continentes_id']));
                    $city->setCountry($country);
                }
            }

            $this->manager->persist($city);
        }
        $this->manager->flush();
    }


    public function migracionContienentePais()
    {
        $countries = $this->manager->getRepository('AppBundle:Country')->findAll();

        foreach($countries as $country)
        {
            //asociar continente con pais
            $continent = $this->manager->getRepository('AppBundle:Continent')->findOneBy(array('id_aereo' => $country->getIdContinent()));

            if ($continent)
            {
                $country->setContinent($continent);
                $this->manager->persist($country);
            }
        }

        $this->manager->flush();

    }

    //6
    public function migracionAeropuertos()
    {
        //por iata
        //8138 registros de aeropuertos

        $RAW_QUERY = "SELECT * FROM rollback_tijetravel.aeropuertos";

        $aeropuertos = $this->aereo->fetchAll($RAW_QUERY);

        foreach($aeropuertos as $aeropuerto)
        {
            $airport = new Airport();
            $airport->setLat($aeropuerto['LATITUDE']);
            $airport->setLon($aeropuerto['LONGITUDE']);
            $airport->setIata($aeropuerto['IATA']);
            $airport->setName($aeropuerto['NAME']);
            $airport->setGeoPointOid($aeropuerto['GEO_POINT_OID']);
            $airport->setGeoAreaOid($aeropuerto['GEO_AREA_OID']);

            $this->manager->persist($airport);

            $city = $this->manager->getRepository('AppBundle:City')->findOneBy(array('iata_from_aereo' => $aeropuerto['City_CODE']));

            if($city)
            {
                $city->addAirport($airport);

                $this->manager->persist($city);
            } else {
                var_dump($aeropuerto['City_CODE']);
            }


        }

        $this->manager->flush();
    }

    //7
    public function migracionZonas()
    {
        $RAW_QUERY = "SELECT hz.*, hd.code
FROM multihoteles.hoteles_zones hz LEFT JOIN multihoteles.hoteles_destinations hd 
ON hz.destinations_id = hd.id";
        $zonas = $this->multiproveedor->fetchAll($RAW_QUERY);

        foreach($zonas as $zona)
        {
            $zone = new Zone();
            $zone->setIdZoneOld($zona['id']); // este campo es que es codZona en la tabla multi_hotels_giata_destino
            $zone->setCode($zona['zoneCode']); //este campo nose para que se usa
            $zone->setName($zona['name']);
            $zone->setDescription($zona['description']);
            $zone->setWeight($zona['ponderacion']);
            $zone->setEnable($zona['habilitado']);

            $city = $this->manager->getRepository('AppBundle:City')->findOneBy(array('iata_from_hotel' => $zona['code']));

            if($city)
            {
                $zone->setCity($city);

                $airports = $city->getAirports();

                if($airports)
                {
                    if(count($airports) == 1)
                    {
                        $airport = $airports[0];

                        $zone->setAirport($airport);

                    } else {

                        $airport_recomended = '';

                        foreach ($airports as $airport)
                        {
                            $airport_recomended.=$airport->getIata();
                        }

                        $zone->setAirportsRecomended($airport_recomended);
                    }
                }
            }

            $this->manager->persist($zone);
        }

        $this->manager->flush();
    }


    public function migracionHoteles()
    {
        //dar alta hoteles directamente

        //$RAW_QUERY = 'SELECT mh.*, m.codZona as codZona, m.codCiudad as codCiudad, m.codPais as codPais
//FROM multihoteles.multi_hotels_giata_destino m
//INNER JOIN multihoteles.multi_hotels mh on mh.id = m.hotelid;';

        //$RAW_QUERY = 'SELECT DISTINCT(m.giata) as giata, m.hotelid as hotelid, m.codZona as codZona, m.codCiudad as codCiudad, m.codPais as codPais, mh.*
//FROM multihoteles.multi_hotels_giata_destino m
//INNER JOIN multihoteles.multi_hotels mh on mh.id = m.hotelid;';

        $RAW_QUERY = 'SELECT m.*, tabla.giata, tabla.codCiudad, tabla.codZona, tabla.codPais
FROM multihoteles.multi_hotels m
INNER JOIN (select giata, hotelid, codCiudad,codZona,codPais, min(id) as minimo, count(id) as cantidad
from multihoteles.multi_hotels_giata_destino  
WHERE giata is not null
group by giata, hotelid, codCiudad, codZona, codPais
having count(id) = 1) AS tabla ON tabla.hotelid = m.id limit 50000;';

        $hoteles = $this->multiproveedor->fetchAll($RAW_QUERY);
        //$i = 1;
        foreach($hoteles as $hotel)
        {

            $hotelNw = new Hotel();
            $hotelNw->setName($hotel['hotels_nombre']);
            $hotelNw->setDireccion($hotel['hotels_direccion']);
            $hotelNw->setLat($hotel['hotels_latitud']);
            $hotelNw->setLon($hotel['hotels_longitud']);
            $hotelNw->setCategory($hotel['hotels_categoria']);
            $hotelNw->setCode($hotel['hotels_code']);
            $hotelNw->setProveedor($hotel['proveedor']);
            $hotelNw->setGiata($hotel['giata']);

            $hotelNw->setCodZone(($hotel['codZona'] != null)? intval($hotel['codZona']) : null );
            $hotelNw->setCodCity(($hotel['codCiudad'] != null) ? intval($hotel['codCiudad']) : null);
            $hotelNw->setCodCountry(($hotel['codPais'] != null ) ? intval($hotel['codPais']) : null);

            $this->manager->persist($hotelNw);

            /*if ($i % 10000 === 0)
            {
                $this->manager->flush();
            }
            $i++;*/

        }

        $this->manager->flush();
    }

    //9
    public function migracionHotelesRelaciones()
    {
        $hoteles = $this->manager->getRepository('AppBundle:Hotel')->findAll();

        foreach($hoteles as $hotel)
        {

            if($hotel->getCodZone() != null)
            {
                $zone = $this->manager->getRepository('AppBundle:Zone')->findOneBy(array('id_zone_old' => $hotel->getCodZone()));

                if($zone)
                {
                    $hotel->setZone($zone);
                } else {
                    var_dump($hotel->getCodZone());
                }

            }

            if($hotel->getCodCity() != null)
            {
                $city = $this->manager->getRepository('AppBundle:City')->findOneBy(array('id_city_old' => $hotel->getCodCity()));

                if ($city) {
                    $hotel->setCity($city);
                } else {
                    var_dump($hotel->getCodCity());
                }
            }

            if ($hotel->getCodCountry() != null )
            {
                $country = $this->manager->getRepository('AppBundle:Country')->findOneBy(array('id_hotel' => $hotel->getCodCountry()));

                if($country){
                    $hotel->setCountry($country);
                } else {
                    var_dump($hotel->getCodCountry());
                }
            }

            $this->manager->persist($hotel);

        }

        $this->manager->flush();
    }

} 
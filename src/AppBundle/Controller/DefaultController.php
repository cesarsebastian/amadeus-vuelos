<?php

namespace AppBundle\Controller;

use Core\MasterPassBundle\DTO\TransactionRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Document\Airport;

use Amadeus\Client;
use Amadeus\Client\Params;
use Amadeus\Client\Result;
use Amadeus\Client\RequestOptions\PnrRetrieveOptions;
use Amadeus\Client\RequestOptions\FareMasterPricerTbSearch;
use Amadeus\Client\RequestOptions\Fare\MPPassenger;
use Amadeus\Client\RequestOptions\Fare\MPItinerary;
use Amadeus\Client\RequestOptions\Fare\MPLocation;
use Amadeus\Client\RequestOptions\Fare\MPDate;

class DefaultController extends Controller
{

    private $multiproveedor;
    private $aereo;
    const PATH_WSDL = '@AppBundle/Resources/doc/';

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //Set up the client with necessary parameters:

        $params = new Params([
            'authParams' => [
                'officeId' => 'BUEG128G5', //The Amadeus Office Id you want to sign in to - must be open on your WSAP.
                'userId' => 'WSTIJTJE', //Also known as 'Originator' for Soap Header 1 & 2 WSDL's
                'passwordData' => 'M0h6QjJRNUI=', // **base 64 encoded** password
                'passwordLength' => 8,
                'dutyCode' => 'SU',
                'organizationId' => 'LATAM',
            ],
            'sessionHandlerParams' => [
                'soapHeaderVersion' => Client::HEADER_V2,
                'wsdl' => $this->container->get('kernel')->locateResource($this::PATH_WSDL . "1ASIWTJETIJ_PRD/AmadeusService.wsdl"),
                //'wsdl' => '/home/user/mytestproject/data/amadeuswsdl/1ASIWXXXXXX_PDT_20110101_080000.wsdl', //Points to the location of the WSDL file for your WSAP. Make sure the associated XSD's are also available.
                //'logger' => new Psr\Log\NullLogger()
            ],
            'requestCreatorParams' => [
                'receivedFrom' => 'my test project' // The "Received From" string that will be visible in PNR History
            ]
        ]);

        $opt = new FareMasterPricerTbSearch([
            'nrOfRequestedResults' => 200,
            'nrOfRequestedPassengers' => 1,
            'passengers' => [
                new MPPassenger([
                    'type' => MPPassenger::TYPE_ADULT,
                    'count' => 1
                ])
            ],
            'itinerary' => [
                new MPItinerary([
                    'departureLocation' => new MPLocation(['city' => 'BRU']),
                    'arrivalLocation' => new MPLocation(['city' => 'LON']),
                    'date' => new MPDate([
                        'dateTime' => new \DateTime('2017-10-15T00:00:00+0000', new \DateTimeZone('UTC'))
                    ])
                ])
            ]
        ]);


        $client = new Client($params);

        $authResult = $client->securityAuthenticate();

        if ($authResult->status === Result::STATUS_OK) {
            //We are authenticated!
            /*$pnrResult = $client->pnrRetrieve(
                new PnrRetrieveOptions(['recordLocator' => 'ABC123'])
            );*/

            $recommendations = $client->fareMasterPricerTravelBoardSearch($opt);

            var_dump($recommendations);

        }

        var_dump('fin');
        exit();

        /*$service = $this->container->get('app.master_pass');

        $transactionRequest = new TransactionRequest();
        $transactionRequest->setTransactionId(449087232);
        $transactionRequest->setAmount(100);
        $transactionRequest->setCurrency("USD");
        $transactionRequest->setApprovalCode("454564545441");

        $response = $service->transaction($transactionRequest);

        var_dump($response);
        exit();*/




        /*$finder = $this->container->get('fos_elastica.finder.app.city');
        $result = $finder->find("buenos");
        var_dump($result);
        exit();*/

        //$dm = $this->get('doctrine_mongodb')->getManager();

        /*$qb = $dm->createQueryBuilder('AppBundle:Client')
            ->select('name');
        $query = $qb->getQuery();
        $client = $query->execute();*/


        //$routes = $dm->getRepository('AppBundle:Client')->findRouteByClientId(6);

        //var_dump($routes);
        //exit();

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }


    public function migracionAeropuertos()
    {
        //$this->multiproveedor = $this->container->get('doctrine.dbal.multiproveedor_connection');

        $this->aereo = $this->container->get('doctrine.dbal.aereo_connection');

        $this->manager = $this->container->get('doctrine_mongodb')->getManager();


        //por iata
        //8138 registros de aeropuertos

        $RAW_QUERY = "SELECT * FROM tijetravel.aeropuertos WHERE City_CODE = '%iata%';";

        //recorro las ciudades

        $cities = $this->manager->getRepository('AppBundle:City')->findAll();

        foreach ($cities as $city)
        {
            $aeropuertos = $this->aereo->fetchAll(str_replace('%iata%', $city->getIata(), $RAW_QUERY));

            foreach($aeropuertos as $aeropuerto)
            {
                $airport = new Airport();
                $airport->setLat($aeropuerto['LATITUDE']);
                $airport->setLon($aeropuerto['LONGITUDE']);
                $airport->setIata($aeropuerto['IATA']);
                $airport->setName($aeropuerto['NAME']);
                $airport->setGeoPointOid($aeropuerto['GEO_POINT_OID']);
                $airport->setGeoAreaOid($aeropuerto['GEO_AREA_OID']);

                $this->manager->persist($airport);

                $city->addAirport($airport);

                $this->manager->persist($city);
            }
        }

        $this->manager->flush();
    }
}

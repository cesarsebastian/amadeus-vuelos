<?php
/**
*  
*/
namespace AppBundle\Services;

class AutocompleteService
{
    private $container = null;

    public function __construct($container = null)
    {
       $this->container = $container;
    }

    public function autocompleteService($request)
    {
        $query = $request->get('query');
        $product = $request->get('product');
        $locale = $request->get('locale');
        $flow_code = $request->get('flow_code');

        $finder = $this->container->get('fos_elastica.finder.app.city');
        $cities = $finder->find($query);

        //$response = count($cities);

        foreach ($cities as $city)
        {
            $r = array(
                'objectId' => $city->getId(),
                'name' => $city->getName(),
                'iata' => $city->getIata(),
                'label' => $city->getNameLabel()
            );
            $response[] = $r;
        }
    }
}
?>